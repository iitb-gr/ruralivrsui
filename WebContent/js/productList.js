$("#page-content").on("click", "#producttable #checkall", function () {
    if ($("#producttable #checkall").is(':checked')) {
        $("#producttable input[type=checkbox]").each(function () {
            $(this).prop("checked", true);
        });
    } else {
        $("#producttable input[type=checkbox]").each(function () {
            $(this).prop("checked", false);
        });
    }
});

//deleting a product entry
$("#page-content").on("click", "#btnYesDelete", function(e) {  
    e.preventDefault();
    $(this).parent('tr').remove();
});
   
//adding new product
$("#page-content").on("click", "#add-new-product", function(e) {
    e.preventDefault();
    var product = $.trim($('#new-product-input').val());
    var price = $.trim($('#new-price-input').val());
    var productType = $('#new-product-type-input').val();
    var data = {};
    data.name = product;
    data.unitRate = price;
    data.productType = productType;

    angular.element($('#add-new-product')).scope().saveProduct(data);

    $('#add-product-modal').modal('toggle');
});

//Dynamic modal for edit
$("#page-content").on("click", ".open-edit-modal", function () {
    var productName = $(this).data('product');
    $(".modal-header #HeadingEdit").html("Edit "+productName+"'s price");
    $(".modal-body #update-product-input").html(productName);
    
    var price = $(this).data('price');
    var priceUpdate = $(this).data('price');
    $(".modal-body #update-price-input").val(price);
    
  //updating a product entry
    $('#update').click(function(e){
    	e.preventDefault();
    	var cost = $("#update-price-input").val();
    	document.getElementById("price0").innerHTML = cost;
    });

});
