'use strict';

var website = angular.module('ruralIvrs', ['ngRoute', 'ngResource', 'angular-loading-bar'])

website.config(['$routeProvider', 'cfpLoadingBarProvider', function($routeProvider, cfpLoadingBarProvider) {

	$routeProvider
		.when('/home', {templateUrl: 'home.html', title: "Home"})
		.when('/products', {templateUrl: 'productList.html', title: "Products"})
		.when('/groups', {templateUrl: 'groupOperations.html', title: "Group Operations"})
		.when('/users', {templateUrl: 'users.html', title: "Users"})
		.when('/settings', {templateUrl: 'settings.html', title: "Settings"})
		.otherwise({redirectTo: '/home', title: "Home"});

	cfpLoadingBarProvider.includeSpinner = false;
}]);

website.factory("Product", function($resource) {
	return $resource("/app/api/products/:id");
});

website.controller("ProductsCtrl", function($scope, $route, Product) {
	$scope.saveProduct = function(data) {
		var product = new Product(data);
		product.$save(function(product) {
			$route.reload();
		}, function(error) {
			$scope.failure = error.data;
			$("#product-add-failed-modal").modal('toggle');
		});
	}
	$scope.deleteProduct = function(data) {
		Product.delete(function(product) {
			$route.reload();
		}, function(error) {
			$scope.failure = error.data;
			$("#product-delete-failed-modal").modal('toggle');
		});
	}
});

website.run(['$location', '$rootScope', function($location, $rootScope) {
	  
	$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
		$rootScope.title = current.$$route.title;
	    });
}]);


